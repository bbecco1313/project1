#ifndef _PARTICLESYSTEM
#define _PARTICLESYSTEM

#pragma once
#include "ofMain.h"

enum particleMode {
	PARTICLE_MODE_ATTRACT = 0,
	PARTICLE_MODE_REPEL,
};

class ParticleSystem {

public:
	ParticleSystem();

	void setMode(particleMode newMode);
	void setAttractPoints(vector <glm::vec2> * attract);

	void reset();
	void update();
	void draw();

	glm::vec2 pos;
	glm::vec2 vel;
	glm::vec2 frc;

	float drag;
	float uniqueVal;
	float scale;

	particleMode mode;

	vector <glm::vec2> * attractPoints;
};
#endif
