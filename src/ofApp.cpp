#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	// Setup grabber
	grabber.setup(1280, 720);

	currentMode = PARTICLE_MODE_ATTRACT;
	modeString = "ATTRACT";

	particleCount = 1000;

	//initalize particle vector
	particles.assign(particleCount, ParticleSystem());

	// Setup tracker
	tracker.setup();

	//initialize the particles
	reset();
}

//--------------------------------------------------------------
void ofApp::update(){
	//update camera
	grabber.update();

	// Update tracker when there are new frames
	if (grabber.isFrameNew()) {
		tracker.update(grabber);
	}

	// Iterate over all faces
	for (auto face : tracker.getInstances()) { //go thru all faces found

		ofxFaceTracker2Landmarks markers = face.getLandmarks(); //get the landmarks for this face

		glm::vec2 lEyeT = markers.getImagePoint(38); //get the points for the top center of each eye
		glm::vec2 rEyeT = markers.getImagePoint(43);
		glm::vec2 lEyeB = markers.getImagePoint(40); //get the points for the top center of each eye
		glm::vec2 rEyeB = markers.getImagePoint(47);
		glm::vec2 mouthTop = markers.getImagePoint(60);
		glm::vec2 mouthBottom = markers.getImagePoint(60);

		float lDist = ofDist(lEyeT.x, lEyeT.y, lEyeB.x, lEyeB.y); //calculate distance from top and bottom of eye
		float rDist = ofDist(rEyeT.x, rEyeT.y, rEyeB.x, rEyeB.y);
		float mDist = ofDist(mouthTop.x, mouthTop.y, mouthBottom.x, mouthBottom.y);

		lEye += (lEyeB - lEye) * .3; //easing the positions
		rEye += (rEyeB - rEye) * .3;
		mouth += (mouthBottom - mouth) * .3;
		mouth.x += 8; //slight manual correction to get center of mouth

		//clear points and update with new position of eyes and mouth
		attractPoints.clear();
		attractPoints.push_back(lEye);
		attractPoints.push_back(rEye);
		attractPoints.push_back(mouth);

		//iterate through vector and update all individual particles
		for (int i = 0; i < particles.size(); i++) {
			particles[i].setMode(currentMode);
			particles[i].update();
			particles[i].setAttractPoints(&attractPoints);
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(0xFFFFFF); //minimize camera tint
	grabber.draw(0, 0); //draw the camera 
	for (int i = 0; i < particles.size(); i++) {
		particles[i].draw();
	}
	//DEBUG PURPOSES
	//ofDrawCircle(lEye, 1);
	//ofDrawCircle(rEye, 1);
	//ofDrawCircle(mouth, 1);
	ofDrawBitmapStringHighlight("PRESS 1 or 2 to toggle ATTRACT/REPEL. CURRENT MODE: " + modeString, 10, 20);
}

void ofApp::reset()
{
	for (int i = 0; i < particles.size(); i++) {
		particles[i].setMode(currentMode);
		particles[i].reset();
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == '1') {
		currentMode = PARTICLE_MODE_ATTRACT;
		modeString = "ATTRACT";
	}
	if (key == '2') {
		currentMode = PARTICLE_MODE_REPEL;
		modeString = "REPEL";
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
