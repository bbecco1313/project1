#include "ParticleSystem.h"
//this class is modified from the particle example in examples/math to implement a particle system

//------------------------------------------------------------------
ParticleSystem::ParticleSystem() {
	attractPoints = NULL;
}

//------------------------------------------------------------------
void ParticleSystem::setMode(particleMode newMode) {
	mode = newMode;
}

//------------------------------------------------------------------
void ParticleSystem::setAttractPoints(vector <glm::vec2> * attract) {
	attractPoints = attract;
}

//------------------------------------------------------------------
void ParticleSystem::reset() {
	uniqueVal = ofRandom(-10000, 10000);

	pos.x = ofRandomWidth();
	pos.y = ofRandomHeight();

	vel.x = ofRandom(-3.9, 3.9);
	vel.y = ofRandom(-3.9, 3.9);

	frc = glm::vec2(0, 0);

	scale = ofRandom(0.5, 1.0);

	drag = ofRandom(0.95, 0.998);
}

//------------------------------------------------------------------
void ParticleSystem::update() {

	//add force to particles

	if (mode == PARTICLE_MODE_ATTRACT) {
		if (attractPoints) {
			//find closest point
			glm::vec2 closestPt;
			int closest = -1;
			float closestDist = 9999999;

			for (int i = 0; i < attractPoints->size(); i++) {
				float lenSq = glm::length2(attractPoints->at(i) - pos);
				if (lenSq < closestDist) {
					closestDist = lenSq;
					closest = i;
				}
			}

			//calcuate the force towards the closest point
			if (closest != -1) {
				closestPt = attractPoints->at(closest);
				float dist = sqrt(closestDist);

				frc = closestPt - pos;

				vel *= drag;

				vel += frc * 0.003;
			}
		}
	}
	else if (mode == PARTICLE_MODE_REPEL) {
		if (attractPoints) {
			//find closest point
			glm::vec2 closestPt;
			int closest = -1;
			float closestDist = 9999999;

			for (int i = 0; i < attractPoints->size(); i++) {
				float lenSq = glm::length2(attractPoints->at(i) - pos);
				if (lenSq < closestDist) {
					closestDist = lenSq;
					closest = i;
				}
			}

			//calculate force
			if (closest != -1) {
				closestPt = attractPoints->at(closest);
				float dist = sqrt(closestDist);

				frc = closestPt - pos;

				vel *= drag;

				//only repel if within a certain distance, add motion with noise otherwise
				if (dist < 300 && dist > 40 && !ofGetKeyPressed('f')) {
					vel += -frc * 0.003; //force is negative to repel
				}
				else {		
					frc.x = ofSignedNoise(uniqueVal, pos.y * 0.01, ofGetElapsedTimef()*0.2);
					frc.y = ofSignedNoise(uniqueVal, pos.x * 0.01, ofGetElapsedTimef()*0.2);
					vel += frc * 0.4;
				}
			}
		}
	}

	//update position
	pos += vel;


	//create bounds so particles stay on screen
	if (pos.x > ofGetWidth()) {
		pos.x = ofGetWidth();
		vel.x *= -1.0;
	}
	else if (pos.x < 0) {
		pos.x = 0;
		vel.x *= -1.0;
	}
	if (pos.y > ofGetHeight()) {
		pos.y = ofGetHeight();
		vel.y *= -1.0;
	}
	else if (pos.y < 0) {
		pos.y = 0;
		vel.y *= -1.0;
	}

}

//------------------------------------------------------------------
void ParticleSystem::draw() {

	if (mode == PARTICLE_MODE_ATTRACT) {
		ofSetColor(77 + ofRandom(-30, 30), 189 + ofRandom(-30, 30), 133 + ofRandom(-30, 30));
	}
	else if (mode == PARTICLE_MODE_REPEL) {
		ofSetColor(245 + ofRandom(-30, 30), 245 + ofRandom(-30, 30), 56 + ofRandom(-30, 30));
	}
	ofSetCircleResolution(10); //reduce resolution to improve frame rate
	ofDrawCircle(pos.x, pos.y, scale * 4.0);
}

