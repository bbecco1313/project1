#ifndef _OFAPP
#define _OFAPP

#pragma once

#include "ofMain.h"
#include "ofxFaceTracker2.h"
#include "ParticleSystem.h"
#include "ofxGui.h"

/*
Blake Becco
Project 1 Advanced Coding
Particle System with Facial Tracking
*/

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void reset();

		ofVideoGrabber grabber;
		ofxFaceTracker2 tracker;

		particleMode currentMode;

		glm::vec2 lEye, rEye, mouth;
		vector <ParticleSystem> particles;
		vector <glm::vec2> attractPoints;
		vector <glm::vec2> attractPointsWithMovement;;

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		int particleCount;
		string modeString;
		
};
#endif
