# Project 1: Particle System with Facial Recognition
This program uses the webcam and creates a particle system. The program reads and recognizes
facial features and the particles in the system will either be attracted to or repelled away from
the features it recognizes based on a user controlled toggle. 
